package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

//avoids configuration of web,xml file for defining servlets.
@WebServlet ("/user")
public class UserServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8689152659639693816L;

	public void init() throws ServletException{
		System.out.println("**************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println(" Connected to database. ");
		System.out.println("**************************************");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		//capture the data via inputs 
		String firstName = req.getParameter("firstName"); 
		String lastName = req.getParameter("lastName"); 
		String email = req.getParameter("email"); 
		String contact = req.getParameter("contact"); 
		
		
			
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("firstName",firstName);
		srvContext.setAttribute("lastName",lastName);
		srvContext.setAttribute("email",email);
		srvContext.setAttribute("contact",contact);
		
		
		//forward request to databaseServlet
//		res.sendRedirect("database");
		res.sendRedirect("database.jsp");
		
		
	}
	public void destroy(){
		System.out.println("**************************************");
		System.out.println(" UserServlet has been destroyed. ");
		System.out.println(" Disconnected to database. ");
		System.out.println("**************************************");
	}
	

}
